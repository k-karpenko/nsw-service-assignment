import sbt.Keys.scalaVersion
import com.typesafe.sbt.SbtNativePackager.autoImport.NativePackagerHelper._

name := "nsw-service-assignment"
version := "0.1"
maintainer := "Cyril Karpenko <cyril@nikelin.ru>"

scalaVersion := "2.12.11"
scalacOptions ++= Seq("-Ypartial-unification")
javaOptions in Universal += "-Duser.timezone=Australia/Sydney"

autoCompilerPlugins := true
scalafmtOnCompile := true
coverageEnabled := true

mappings in Universal ++= directory(baseDirectory.value / "src" / "main" / "resources" / "fixtures").map { pair =>
  pair.copy(_2 = "conf/" + pair._2)
}

mappings in Universal +=
  baseDirectory.value / "src" / "main" / "resources" / "application.conf" -> "conf/application.conf"

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full)

enablePlugins(JavaServerAppPackaging)

libraryDependencies ++=
  Dependencies.fs2 ++
    Dependencies.doobie ++
    Dependencies.common ++
    Dependencies.http4s ++
    Dependencies.circe ++
    Dependencies.logging ++
    Dependencies.tests