package au.gov.nsw.service.backend

package object config {
  case class Password(value: String) extends AnyVal {
    override def toString = "PASSWORD"
  }

  case class DatabaseConfig(driverClassName: String, jdbcUri: String, userName: String, password: Password, maxConnectionsPoolSize: Int)

  case class HttpServiceConfig(servicePort: Int, serviceHost: String)

  case class ApplicationConfig(enableSchemaAutoSetup: Boolean)
}
