package au.gov.nsw.service.backend

import java.util.concurrent.Executors

import cats.effect._
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import pureconfig._
import pureconfig.generic.auto._
import org.http4s.server.blaze.BlazeServerBuilder
import services._
import db._
import db.schema._
import config._
import doobie.implicits._

import scala.io.{ Codec, Source }

object Application extends IOApp with LazyLogging {

  val dbConfig: DatabaseConfig = ConfigSource.default
    .at("database")
    .loadOrThrow[DatabaseConfig]

  val httpConfig: HttpServiceConfig = ConfigSource.default
    .at("http")
    .loadOrThrow[HttpServiceConfig]

  val appConfig: ApplicationConfig = ConfigSource.default
    .at("application")
    .loadOrThrow[ApplicationConfig]

  implicit val codec: Codec  = Codec.UTF8
  implicit val db: DbService = new DbServiceImpl()

  override def run(args: List[String]): IO[ExitCode] =
    buildTransactor(dbConfig).use { implicit tx =>
      val schemaSetupCheck: IO[Unit] =
        if (appConfig.enableSchemaAutoSetup)
          setupSchema
            .transact(tx)
            .flatMap(_ => IO(logger.info("[schema] OK - Schema setup complete.")))
        else IO(logger.info("[schema] Skipping schema setup..."))

      val initialDataLoad: IO[Unit] =
        args.headOption match {
          case Some(x) if x.startsWith("--initialData=") =>
            loadFixtures(Source.fromFile(x.substring("--initialData=".length)))
              .transact(tx)
              .flatMap(_ => IO(logger.info("[fixtures] OK - Initial data loaded.")))
          case _                                         =>
            IO(logger.warn("No initial data source provided"))
        }

      schemaSetupCheck flatMap { _ =>
        initialDataLoad flatMap { _ =>
          BlazeServerBuilder[IO]
            .bindHttp(httpConfig.servicePort, httpConfig.serviceHost)
            .withHttpApp(appRouter)
            .serve
            .compile
            .drain
            .as(ExitCode.Success)
        }
      }
    }
}
