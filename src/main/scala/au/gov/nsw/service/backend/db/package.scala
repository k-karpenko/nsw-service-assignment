package au.gov.nsw.service.backend

import java.util.concurrent.Executors

import au.gov.nsw.service.backend.config.DatabaseConfig
import cats.effect.{ Blocker, ConcurrentEffect, ContextShift, IO, Resource }
import doobie.h2.H2Transactor
import doobie.util.transactor.Transactor

import scala.concurrent.ExecutionContext

package object db {
  def buildTransactor(databaseConfig: DatabaseConfig)(implicit
      concurrent: ConcurrentEffect[IO],
      shift: ContextShift[IO]
  ): Resource[IO, Transactor[IO]] = {
    val dbTransactExecutionContext: ExecutionContext = ExecutionContext.fromExecutor {
      Executors.newCachedThreadPool
    }

    val dbExecutionContext: ExecutionContext = ExecutionContext.fromExecutor {
      Executors.newFixedThreadPool(databaseConfig.maxConnectionsPoolSize)
    }

    H2Transactor.newH2Transactor(
      databaseConfig.jdbcUri,
      databaseConfig.userName,
      databaseConfig.password.value,
      dbExecutionContext,
      Blocker.liftExecutionContext(dbTransactExecutionContext)
    )
  }

}
