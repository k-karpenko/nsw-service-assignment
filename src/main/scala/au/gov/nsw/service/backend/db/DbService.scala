package au.gov.nsw.service.backend.db

import au.gov.nsw.service.backend.domain.Registration
import doobie.free.connection.ConnectionIO
import fs2.Stream

trait DbService {
  def fetchRegistrations(): Stream[ConnectionIO, Registration]
}
