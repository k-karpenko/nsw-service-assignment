package au.gov.nsw.service.backend

import java.time.{ LocalDate, LocalDateTime, ZonedDateTime }

import io.circe.Encoder
import io.circe.generic.extras.semiauto._
import io.circe.generic.extras._

package object domain {
  implicit val config: Configuration = Configuration.default.withSnakeCaseMemberNames

  object Vehicle {
    implicit val encoder: Encoder[Vehicle] = deriveConfiguredEncoder[Vehicle]
  }

  case class Vehicle(`type`: String, vin: Long, model: String, make: String, colour: String, tareWeight: Int, grossMass: Option[Int])

  object Insurer {
    implicit val encoder: Encoder[Insurer] = deriveConfiguredEncoder[Insurer]
  }

  case class Insurer(code: Int, name: String)

  object Expiration {
    implicit val encoder: Encoder[Expiration] = deriveConfiguredEncoder[Expiration]

    def apply(value: ZonedDateTime): Expiration =
      Expiration(ZonedDateTime.now.isBefore(value), value)
  }

  case class Expiration(expired: Boolean, expiryDate: ZonedDateTime)

  object Registration {
    implicit val encoder: Encoder[Registration] = deriveConfiguredEncoder[Registration]
  }

  case class Registration(plateNumber: String, registration: Expiration, vehicle: Vehicle, insurer: Insurer)
}
