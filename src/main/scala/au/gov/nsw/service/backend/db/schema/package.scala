package au.gov.nsw.service.backend.db

import doobie._
import doobie.implicits._

import scala.io.BufferedSource
import doobie.implicits._
import cats.implicits._

package object schema {
  def setupSchema: ConnectionIO[Unit] = {
    val vehicleSql = sql"""
      create table vehicle (
        tpe varchar(128) not null,
        vin bigint primary key,
        make varchar(256) not null,
        colour varchar(128) not null,
        model_name varchar(256) not null,
        tare_weight int not null,
        gross_mass int
      );
    """

    val insurerSql =
      sql"""
        create table insurer (
            code int primary key,
            name varchar(1024) not null,
            unique (name)
        );
    """

    val registrationSql =
      sql"""
        create table registration (
            plate_number varchar(6) not null,
            expiry_date timestamp not null,
            vin bigint not null,
            insurer_code int not null,
            foreign key (vin) references vehicle (vin) on delete restrict,
            foreign key (insurer_code) references insurer (code) on delete restrict
        )
      """

    val statement = for {
      _ <- insurerSql.update.run
      _ <- vehicleSql.update.run
      _ <- registrationSql.update.run
    } yield {}

    statement
  }

  def loadFixtures(source: BufferedSource): ConnectionIO[Unit] = {
    val ios: List[ConnectionIO[Int]] =
      source.mkString.split(";").toList.map { sql =>
        Fragment.const(sql + ";").update.run
      }

    ios.sequence_
  }
}
