package au.gov.nsw.service.backend

import au.gov.nsw.service.backend.db.DbService
import cats.data.Kleisli
import cats.effect.IO
import doobie.util.transactor.Transactor
import org.http4s.{ HttpRoutes, Request, Response }
import org.http4s.dsl.impl.Root
import org.http4s.implicits._
import org.http4s.server.Router
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.dsl.io._
import doobie.implicits._
import io.circe.Json
import org.http4s.server.Router

package object services {
  def apiHttpService(implicit transactor: Transactor[IO], db: DbService): HttpRoutes[IO] =
    HttpRoutes.of[IO] {
      case GET -> Root =>
        val io = db.fetchRegistrations().compile.toList.transact(transactor)

        io flatMap { lst =>
          Ok(
            Json.obj(("registrations", lst.asJson)).spaces4
          )
        }
    }

  def appRouter()(implicit transactor: Transactor[IO], db: DbService): Kleisli[IO, Request[IO], Response[IO]] =
    Router(
      "/api" -> apiHttpService
    ).orNotFound
}
