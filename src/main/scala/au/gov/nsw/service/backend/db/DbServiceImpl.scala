package au.gov.nsw.service.backend.db

import java.time.ZonedDateTime

import cats.implicits._
import doobie._
import doobie.implicits._
import au.gov.nsw.service.backend.domain.{ Expiration, Insurer, Registration, Vehicle }
import doobie._
import doobie.implicits._
import fs2.Stream
import doobie.implicits.javatime._

class DbServiceImpl extends DbService {

  override def fetchRegistrations(): Stream[ConnectionIO, Registration] = {
    type QueryResult = (String, ZonedDateTime, String, Long, String, String, String, Int, Option[Int], Int, String)
    val sql = sql"select r.plate_number, r.expiry_date, " ++
          sql" v.tpe as vehicle_tpe, v.vin, v.make, v.colour, v.model_name, v.tare_weight, v.gross_mass," ++
          sql" i.code, i.name" ++
          sql" from registration r" ++
          sql" left join vehicle v on v.vin = r.vin" ++
          sql" left join insurer i on i.code = r.insurer_code"

    implicit val read = Read[QueryResult]
    sql
      .query[QueryResult]
      .stream
      .map {
        case (plateNumber, expiryDate, tpe, vin, make, colour, model_name, tare_weight, gross_mass, code, name) =>
          Registration(
            plateNumber,
            Expiration(expiryDate),
            Vehicle(tpe, vin, make, colour, model_name, tare_weight, gross_mass),
            Insurer(code, name)
          )
      }
  }
}
