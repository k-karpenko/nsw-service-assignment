insert into vehicle (tpe, make, model_name, colour, vin, tare_weight, gross_mass)
    values ('Wagon', 'BMW', 'X4 M40i', 'Blue', 12389347324, 1700, null),
    ('Hatch', 'Toyota', 'Corolla', 'Silver', 54646546313, 1432, 1500),
    ('Sedan', 'Mercedes', 'X4 M40i', 'Blue', 87676676762, 1700, null),
    ('SUV', 'Jaguar', 'F pace', 'Green', 65465466541, 1620, null);

insert into insurer (code, name) values (32, 'Allianz'), (17, 'AAMI'), (13, 'GIO'), (27, 'NRMA');

insert into registration (plate_number, expiry_date, insurer_code, vin)
    values ('EBF28E', '2021-02-05T23:15:30.000Z', 32, 12389347324),
    ('CXD82F', '2020-03-01T23:15:30.000Z', 17, 54646546313),
    ('WOP29P', '2020-12-08T23:15:30.000Z', 13, 87676676762),
    ('QWX55Z', '2021-07-20T23:15:30.000Z', 27, 65465466541),
    ('QWX55Z', '2019-07-20T23:15:30.000Z', 27, 65465466541),
    ('QWX55A', '2015-07-20T23:15:30.000Z', 32, 65465466541);