package au.gov.nsw.service.backend.spec

import au.gov.nsw.service.backend.{ db, domain, DbTest }
import org.scalatest.matchers.should.Matchers
import au.gov.nsw.service.backend.db._
import cats.effect.IO
import doobie.implicits._
import org.scalatest.wordspec.AnyWordSpec

class DbServiceSpec extends AnyWordSpec with Matchers with DbTest {

  "fetchRegistrations" must {
    "return a stream of all records in the database" in new Fixture {
      val ioResult: IO[List[domain.Registration]] =
        withTransactor { implicit tx =>
          withSchema.flatMap { _ =>
            withFixtures("test-records.sql").flatMap { _ =>
              dbService
                .fetchRegistrations()
                .compile
                .toList
                .transact(tx)
            }
          }
        }

      ioResult.unsafeRunSync().size shouldBe 6
    }

    "return an empty stream if there are no records available" in new Fixture {
      val ioResult: IO[List[domain.Registration]] =
        withTransactor { implicit tx =>
          withSchema flatMap { _ =>
            dbService
              .fetchRegistrations()
              .compile
              .toList
              .transact(tx)
          }
        }

      ioResult.unsafeRunSync().size shouldBe 0
    }
  }

  trait Fixture {
    val dbService = new DbServiceImpl
  }

}
