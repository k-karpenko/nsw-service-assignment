package au.gov.nsw.service.backend

import au.gov.nsw.service.backend.config.DatabaseConfig
import au.gov.nsw.service.backend.db.buildTransactor
import au.gov.nsw.service.backend.db.schema.{ loadFixtures, setupSchema }
import cats.effect.{ Blocker, IO, Resource }
import doobie.util.transactor.Transactor
import doobie._
import doobie.implicits._
import pureconfig.ConfigSource
import pureconfig.generic.auto._

import scala.io.Source

trait DbTest extends IOTest {
  val databaseConfig: DatabaseConfig                = ConfigSource.default.at("database").loadOrThrow[DatabaseConfig]
  lazy val transactor: Resource[IO, Transactor[IO]] = buildTransactor(databaseConfig)

  def withNoopTransactor[T](implicit blocker: Blocker): Transactor[IO] = {
    val s = doobie.util.transactor.Strategy(FC.unit, FC.unit, FC.unit, FC.unit)
    Transactor.fromConnection[IO](null, blocker).copy(strategy0 = s)
  }

  def withTransactor[T](fn: Transactor[IO] => IO[T]): IO[T] =
    transactor.use { tx =>
      fn(tx)
    }

  def withSchema(implicit tx: Transactor[IO]): IO[Unit] =
    setupSchema.transact(tx)

  def withFixtures(path: String)(implicit tx: Transactor[IO]): IO[Unit] =
    loadFixtures(Source.fromResource(path)).transact(tx)

}
