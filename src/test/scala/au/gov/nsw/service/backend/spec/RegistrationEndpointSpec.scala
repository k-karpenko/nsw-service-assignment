package au.gov.nsw.service.backend.spec

import java.time.ZonedDateTime
import java.util.concurrent.Executors

import au.gov.nsw.service.backend.db.DbService
import au.gov.nsw.service.backend.domain.{ Expiration, Insurer, Registration, Vehicle }
import au.gov.nsw.service.backend.{ DbTest, HttpTest }
import au.gov.nsw.service.backend.services._
import cats.effect.{ Blocker, IO }
import doobie.util.transactor.Transactor
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.mockito._
import io.circe._
import io.circe.syntax._
import org.http4s._
import org.http4s.circe._
import org.http4s.implicits._

class RegistrationEndpointSpec extends AnyWordSpec with MockitoSugar with HttpTest with DbTest with Matchers {

  "apiHttpService" must {
    "produce correct JSON data for all available registration records" in new Fixture {
      implicit val tx        = withNoopTransactor
      implicit val dbService = mock[DbService]

      when(dbService.fetchRegistrations()).thenReturn(fs2.Stream(testRegistrations: _*))

      val response = apiHttpService.orNotFound.run(Request(method = Method.GET, uri = uri"/"))

      val expectedJson = Json.obj(("registrations", testRegistrations.asJson))

      check[Json](response, Status.Ok, Some(expectedJson))
    }

    "produce correct JSON data if there are no registration records available" in new Fixture {
      implicit val tx: Transactor[IO] = withNoopTransactor

      implicit val dbService: DbService = mock[DbService]

      when(dbService.fetchRegistrations()).thenReturn(fs2.Stream.empty)

      val response: IO[Response[IO]] = apiHttpService.orNotFound.run(Request(method = Method.GET, uri = uri"/"))

      val expectedJson = Json.obj(("registrations", Json.arr()))

      check[Json](response, Status.Ok, Some(expectedJson))
    }
  }

  trait Fixture {
    val testVehicle1 = Vehicle("ttt", 111111, "aaaa", "b", "c", 1000, Some(2000))
    val testVehicle2 = Vehicle("t9", 398222, "vcc", "b", "c", 1000, None)

    val testInsurer1 = Insurer(32, "NRMA")
    val testInsurer2 = Insurer(17, "AIA")

    val testRegistrations         = Seq(
      Registration("AAAAA", Expiration(ZonedDateTime.now), testVehicle1, testInsurer1),
      Registration("AAAAA", Expiration(ZonedDateTime.now), testVehicle2, testInsurer2)
    )
    implicit val blocker: Blocker = Blocker.liftExecutorService(Executors.newFixedThreadPool(4))
  }

}
