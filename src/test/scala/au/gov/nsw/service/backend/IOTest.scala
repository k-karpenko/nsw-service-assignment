package au.gov.nsw.service.backend

import cats.effect.{ ConcurrentEffect, ContextShift, IO, Timer }

import scala.concurrent.{ ExecutionContext, ExecutionContextExecutor }
import scala.io.Codec

trait IOTest {
  implicit val codec: Codec                               = Codec.UTF8
  implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global
  implicit val timer: Timer[IO]                           = IO.timer(executionContext)
  implicit val contextShift: ContextShift[IO]             = IO.contextShift(executionContext)
  implicit val concurrent: ConcurrentEffect[IO]           = IO.ioConcurrentEffect(contextShift)
}
