# Vehicle Registrations API

## Overview

The project is a simple JSON HTTP endpoint implementation developed in Scala 2.12. 

## Prerequisites

  * SBT: https://www.scala-sbt.org/download.html
  * JDK: https://docs.aws.amazon.com/corretto/latest/corretto-8-ug/downloads-list.html

## Dependencies

  * Runtime
    * logback 1.2.3 
    * scalaLogging 3.5.0
    * circe 0.12.3
    * http4s 0.21.3
    * doobie 0.9.0
    * fs2 2.3.0
    * pureconfig 0.12.3
  * Test
    * scalatest 3.0.5
    * mockito 1.1.1


## Test

The command presented below will result in execution of all tests in the project and a test coverage report generation:
```
sbt clean coverage test
``` 

To generate the test coverage report run
```
sbt coverageReport
```

Coverage report will be available in 
```
./target/scala-2.12/scoverage-report/index.html
```

## Build 

The command below will generate a distributable artifact:

```
sbt  universal:packageBin
```

The resulting artifact will be located at:
```
$root/target/nsw-service-assignment-0.1.zip
```

## Run

There are two options to test application: to start it from the binary distributive or to run directly from the source code repository (**SBT**).

1. **Option 1** - Start from the binary package (see **Build**)

    ```
    $pathToServerDist/bin/nsw-service-assignment
    ```

2. **Option 2** - Run from the source code
    ```
    sbt run
    ```
   
The application can be configured either by changing its configuration file available at `src/main/resources/application.conf`, 
or at `./conf/application.conf` in case of a binary distributive. 

It is possible to override the application configuration via environment variables. Down below is the list of all
environment variables supported by the application:
  * **HTTP_SERVER_HOST** (`string`, i.e. `0.0.0.0`) - a hostname used by the application to start the HTTP server 
  * **HTTP_SERVER_PORT** (`integer`, i.e. `8080`) - a port number used by the application to start the HTTP server
  * **ENABLE_SCHEMA_AUTOSETUP** (`boolean`) - if enabled the application attempts to set up the schema on its startup 
  * **JDBC_DRIVER_CLASSNAME** (`string`, i.e. `org.h2.Driver`) - a name of the class to be used as a JDBC driver implementation
  * **JDBC_URI** (`string`, i.e. `jdbc:h2:mem:testdb`) - a JDBC connection string 
  * **DB_USER_NAME** (`string`, i.e. `sa`) - a DB username
  * **DB_USER_PASSWORD** (`string`, i.e. ` `) - a DB password
  * **DB_MAX_CONNECTIONS_POOL_SIZE** (`int`, i.e. `32`) - a thread pool size allocated for DB connections management   

Also, it is possible to supply the initial fixtures to the application on its start using the flag `--initialData` like it is done in the example presented below:
```
./bin/nsw-service-assignment --initialData=fixtures/1.sql
``` 

The only requirement for the fixtures file is that each separate SQL statement in it must be followed by `;`.  

## Database schema

The database schema definition is available here:
```
src\main\scala\au\gov\nsw\service\backend\db\schema\package.scala
```

## Endpoints

At the moment there is only a single API endpoint available:
```
    GET /api
```

Example request and expected response (based on the initial data provided in the project):
```
Request URL: /api
Request Method: GET

Response:
{
    "registrations" : [
        {
            "plate_number" : "EBF28E",
            "registration" : {
                "expired" : true,
                "expiry_date" : "2021-02-06T10:15:30+11:00"
            },
            "vehicle" : {
                "type" : "Wagon",
                "vin" : 12389347324,
                "model" : "BMW",
                "make" : "Blue",
                "colour" : "X4 M40i",
                "tare_weight" : 1700,
                "gross_mass" : null
            },
            "insurer" : {
                "code" : 32,
                "name" : "Allianz"
            }
        },
        {
            "plate_number" : "CXD82F",
            "registration" : {
                "expired" : false,
                "expiry_date" : "2020-03-02T10:15:30+11:00"
            },
            "vehicle" : {
                "type" : "Hatch",
                "vin" : 54646546313,
                "model" : "Toyota",
                "make" : "Silver",
                "colour" : "Corolla",
                "tare_weight" : 1432,
                "gross_mass" : 1500
            },
            "insurer" : {
                "code" : 17,
                "name" : "AAMI"
            }
        },
        {
            "plate_number" : "WOP29P",
            "registration" : {
                "expired" : true,
                "expiry_date" : "2020-12-09T10:15:30+11:00"
            },
            "vehicle" : {
                "type" : "Sedan",
                "vin" : 87676676762,
                "model" : "Mercedes",
                "make" : "Blue",
                "colour" : "X4 M40i",
                "tare_weight" : 1700,
                "gross_mass" : null
            },
            "insurer" : {
                "code" : 13,
                "name" : "GIO"
            }
        },
        {
            "plate_number" : "QWX55Z",
            "registration" : {
                "expired" : true,
                "expiry_date" : "2021-07-21T09:15:30+10:00"
            },
            "vehicle" : {
                "type" : "SUV",
                "vin" : 65465466541,
                "model" : "Jaguar",
                "make" : "Green",
                "colour" : "F pace",
                "tare_weight" : 1620,
                "gross_mass" : null
            },
            "insurer" : {
                "code" : 27,
                "name" : "NRMA"
            }
        }
    ]
}


```
