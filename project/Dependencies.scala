import sbt._

object Dependencies {

  val common = Seq(
    "com.github.pureconfig" %% "pureconfig" % Version.pureconfig
  )

  val logging = Seq(
    "ch.qos.logback" % "logback-classic" % Version.logback,
    "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  )

  val tests = Seq(
    "org.scalatest" %% "scalatest" % Version.scalatest % Test,
    "org.mockito" %% "mockito-scala" % Version.mockito % Test
  )

  val circe = Seq(
    "io.circe" %% "circe-core" % Version.circe,
    "io.circe" %% "circe-generic" % Version.circe,
    "io.circe" %% "circe-generic-extras" % Version.circeExtras,
    "io.circe" %% "circe-parser" % Version.circe
  )

  val http4s = Seq(
    "org.http4s" %% "http4s-dsl" % Version.http4s,
    "org.http4s" %% "http4s-circe" % Version.http4s,
    "org.http4s" %% "http4s-blaze-server" % Version.http4s,
    "org.http4s" %% "http4s-blaze-client" % Version.http4s
  )

  val fs2 = Seq(
    "co.fs2" %% "fs2-core" % Version.fs2,
    "co.fs2" %% "fs2-io" % Version.fs2
  )

  val doobie = Seq(
    "org.tpolecat" %% "doobie-core"      % Version.doobie,
    "org.tpolecat" %% "doobie-h2" % Version.doobie,
    "org.tpolecat" %% "doobie-scalatest" % Version.doobie
  )

}
