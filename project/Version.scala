object Version {
  val logback = "1.2.3"
  val scalaLogging = "3.5.0"
  val scalatest = "3.0.5"
  val mockito = "1.1.1"
  val circe = "0.12.3"
  val circeExtras = "0.12.1"
  val http4s = "0.21.3"
  val doobie = "0.9.0"
  val fs2 = "2.3.0"
  val pureconfig = "0.12.3"
}
